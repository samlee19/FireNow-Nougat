package com.android.server.firefly;
import android.util.Log;
import android.os.IFireflyManager;
import android.content.Context;
import android.os.PowerManager;
import android.os.HandlerThread;
import android.content.Intent;
import android.firefly.util.SystemUtils;
import android.view.IWindowManager;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import android.os.RemoteException;
import com.android.internal.statusbar.IStatusBarService;
import android.os.ServiceManager;

public class FireflyManagerServiceImpl extends IFireflyManager.Stub {
    private static final String TAG = "FireflyManagerServiceImpl";

    public static final int FIREFLY_POWER_CONTROL_REBOOT = 1; 
    public static final int FIREFLY_POWER_CONTROL_SHUTDOWN = 2; 
    public static final int FIREFLY_POWER_CONTROL_SLEEP = 3; 

   PowerManager mPowerManager;
    Context context;
    public FireflyManagerServiceImpl(Context context) {
        Log.d(TAG,"IFireflyManagerService is create");
        this.context = context;
          Log.d(TAG,"context:"+(context == null));
          mPowerManager =(PowerManager) context.getSystemService(Context.POWER_SERVICE);    
       
    }

    public void reboot(boolean confirm,String reason)
    {
         mPowerManager.fireflyControl(FIREFLY_POWER_CONTROL_REBOOT,confirm,reason,true);
    }

    public void shutdown(boolean confirm)
    {
         mPowerManager.fireflyControl(FIREFLY_POWER_CONTROL_SHUTDOWN,confirm,null,true);
    }

    public void sleep(){
          mPowerManager.fireflyControl(FIREFLY_POWER_CONTROL_SLEEP,false,null,false);
    }


    /*******
     * 
     * 系统设置
     * 
     */
    public  void  takeScreenshot(String path,String name)
    {
          SystemUtils.takeScreenshot(context, path, name);
    }


    public  void setRotation (int  rotation){
      try {
        IWindowManager wm = WindowManagerGlobal.getWindowManagerService();
        wm.fireflyFreezeRotation(rotation);
      } catch (RemoteException e) {
      // TODO Auto-generated catch block
        e.printStackTrace();
    }

  } 

    public  int getRotation(){
      try {
        IWindowManager wm = WindowManagerGlobal.getWindowManagerService();
        return wm.getRotation();
      } catch (RemoteException e) {
      // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return 0;
    }
    public  void thawRotation ( ){
      try {
        IWindowManager wm = WindowManagerGlobal.getWindowManagerService();
        wm.fireflyThawRotation();
      } catch (RemoteException e) {
      // TODO Auto-generated catch block
        e.printStackTrace();
      }

    } 


    public void setStatusBar(boolean show){
         try {
            IStatusBarService statusbar = IStatusBarService.Stub.asInterface(
                        ServiceManager.getService("statusbar"));;
            if (statusbar != null) {
                if(show)
                  statusbar.addBar();
                else
                  statusbar.removeBar();
            }
        } catch (RemoteException e) {
        }
    }


    public void start() {
        Log.i(TAG, "Starting FireflyManager Service");

        HandlerThread handlerThread = new HandlerThread("FireflyManagerServiceThread");
        handlerThread.start();
    }
}